# log-to-defmt

This is a logging adapter that acts as an implementation of the [log crate] and hands the
rendered messages off to the [defmt crate].

Using this is generally not recommended: Once the log infrastructure is pulled into a project,
Rust's string formatting is brought in, avoiding which is a big part of the point of defmt.

However, this can be useful during development, when debugging a particular library (that
optionally produces messages through log) on a platform for which a defmt output has already
been established.

[log crate]: https://crates.io/crates/log
[defmt crate]: https://crates.io/crates/defmt

#### Maturity

The current implementation of this takes a huge amount of shortcuts: not only does it not
convert log levels, it also hardwires some to the most verbose level, discards lots of
information that would otherwise be available, and uses a fixed size buffer.

Future iterations of this crate are expected to address these on demand; for example, this
could gain a build time configuration mechanism for the maximum expected length, or an `alloc`
feature that renders the log messages to a `Vec` instead of a `heapless::Vec` before handing
them off to defmt as a slice.

The crate will likely introduce such features (altering its behavior) without declaring
breaking changes; users are advised to configure their error levels to match which messages
they expect to see.

License: MIT OR Apache-2.0
